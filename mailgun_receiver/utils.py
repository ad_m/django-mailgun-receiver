import requests
from django.conf import settings
from email.Utils import formatdate
from .models import Update, Message, Attachment
from time import mktime
from django.dispatch import Signal
from django.core.files import File
from email.utils import parsedate
from datetime import datetime

store_message_signal = Signal(providing_args=["r", "msg", "att_list"])


def get(url, s=requests.Session(), *args, **kwargs):
    if not 'api.mailgun.net' in url:
        url = "https://api.mailgun.net/v2/%s/%s" % (settings.MAILGUN['DOMAIN'],
                                                    url.strip('/'))
    r = s.get(url, auth=('api', settings.MAILGUN['API_KEY']), *args, **kwargs)
    print r.url, kwargs.get('params', '')
    return r


def save_msg(url, update, s=requests.Session()):
    r = get(url, s=s).json()
    json2model = {'Date': 'date',
                'From': 'from_header',
                'Message-Id': 'message_id',
                'Subject': 'subject',
                'To': 'to_header',
                'From': 'from_header',
                'message-headers': 'message_headers',
                'recipients': 'recipients',
                'sender': 'sender',
                'stripped-html': 'stripped_html',
                'stripped-signature': 'stripped_signature',
                'stripped-text': 'stripped_text',
                'subject': 'subject',
                'body-html': 'body_html',
                'body-plain': 'body_plain'}
    params = {v: r.get(k) for k, v in json2model.items()}
    params['date'] = datetime.fromtimestamp(mktime(parsedate(params['date'])))
    msg = Message(update=update, **params)
    msg.save()
    if 'attachments' in r:
        att_list = []
        for a in r['attachments']:
            r_att = get(a['url'], s=s)
            r_att.raw.decode_content = True
            att_list.append(Attachment(f=File(r_att),
                                       filename=a['name'],
                                       content_type=a['content-type'],
                                       message=msg))
        Attachment.objects.bulk_create(att_list)
    store_message_signal.send(sender='save_msg', r=r, msg=msg, att_list=att_list)
    print msg


def fetch():
    s = requests.Session()

    try:
        latest = Update.objects.latest()
        params = {'end': formatdate(mktime(latest.created_on.timetuple()))}
    except Update.DoesNotExist:
        params = {}
    params['event'] = 'stored'

    current = Update()
    current.save()

    init = True
    r = get('events/', params=params, s=s).json()

    while init or r['items']:  # do_while loop
        init = False
        for msg in r['items']:
            save_msg(msg['storage']['url'], current)
        r = get(r['paging']['next'], s=s).json()
