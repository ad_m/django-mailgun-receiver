from django.core.management.base import BaseCommand
from mailgun_receiver.utils import fetch


class Command(BaseCommand):
    help = 'Download mail from mailgun'

    def handle(self, *args, **options):
        fetch()