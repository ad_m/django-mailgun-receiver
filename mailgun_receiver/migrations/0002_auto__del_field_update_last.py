# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Update.last'
        db.delete_column(u'mailgun_receiver_update', 'last')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Update.last'
        raise RuntimeError("Cannot reverse this migration. 'Update.last' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Update.last'
        db.add_column(u'mailgun_receiver_update', 'last',
                      self.gf('django.db.models.fields.DecimalField')(max_digits=13, decimal_places=7),
                      keep_default=False)


    models = {
        u'mailgun_receiver.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'content_type': ('django.db.models.fields.TextField', [], {'max_length': '15'}),
            'f': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Message']"})
        },
        u'mailgun_receiver.message': {
            'Meta': {'object_name': 'Message'},
            'body_html': ('django.db.models.fields.TextField', [], {}),
            'body_plain': ('django.db.models.fields.TextField', [], {}),
            'from_header': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'recipient': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripped_html': ('django.db.models.fields.TextField', [], {}),
            'stripped_signature': ('django.db.models.fields.TextField', [], {}),
            'stripped_text': ('django.db.models.fields.TextField', [], {}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'update': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Update']"})
        },
        u'mailgun_receiver.update': {
            'Meta': {'object_name': 'Update'},
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['mailgun_receiver']