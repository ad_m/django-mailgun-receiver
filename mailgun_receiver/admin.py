from django.contrib import admin
from .models import Message, Update, Attachment
from django.db.models import Count


class UpdateAdmin(admin.ModelAdmin):
    pass
admin.site.register(Update, UpdateAdmin)


class AttachmentInline(admin.TabularInline):
    model = Attachment


class MessageAdmin(admin.ModelAdmin):
    list_display = ('subject', 'recipients', 'date', 'sender', 'to_header',
        'from_header', 'update', 'attachment_count')
    inlines = [AttachmentInline]

    def queryset(self, request):
        qs = super(MessageAdmin, self).queryset(request)
        return qs.annotate(attachment_count=Count('attachment'))

    def attachment_count(self, obj):
        return obj.attachment_count
    attachment_count.short_description = 'Attachment count'
    attachment_count.admin_order_field = 'attachment_count'
admin.site.register(Message, MessageAdmin)


class AttachmentAdmin(admin.ModelAdmin):
    list_display = ('filename', 'message', 'content_type')
admin.site.register(Attachment, AttachmentAdmin)
