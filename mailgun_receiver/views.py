from django.http import HttpResponse
import json
from .utils import fetch


def callback(request):
    fetch()
    return HttpResponse(json.dumps({'status': 'OK'}))
