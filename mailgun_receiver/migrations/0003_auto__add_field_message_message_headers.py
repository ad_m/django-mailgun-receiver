# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Message.message_headers'
        db.add_column(u'mailgun_receiver_message', 'message_headers',
                      self.gf('jsonfield.fields.JSONField')(default={}),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Message.message_headers'
        db.delete_column(u'mailgun_receiver_message', 'message_headers')


    models = {
        u'mailgun_receiver.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'content_type': ('django.db.models.fields.TextField', [], {'max_length': '15'}),
            'f': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Message']"})
        },
        u'mailgun_receiver.message': {
            'Meta': {'object_name': 'Message'},
            'body_html': ('django.db.models.fields.TextField', [], {}),
            'body_plain': ('django.db.models.fields.TextField', [], {}),
            'from_header': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_headers': ('jsonfield.fields.JSONField', [], {'default': '{}'}),
            'message_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'recipient': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripped_html': ('django.db.models.fields.TextField', [], {}),
            'stripped_signature': ('django.db.models.fields.TextField', [], {}),
            'stripped_text': ('django.db.models.fields.TextField', [], {}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'update': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Update']"})
        },
        u'mailgun_receiver.update': {
            'Meta': {'object_name': 'Update'},
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['mailgun_receiver']