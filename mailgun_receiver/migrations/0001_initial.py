# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Update'
        db.create_table(u'mailgun_receiver_update', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('last', self.gf('django.db.models.fields.DecimalField')(max_digits=13, decimal_places=7)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'mailgun_receiver', ['Update'])

        # Adding model 'Message'
        db.create_table(u'mailgun_receiver_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message_id', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('recipient', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('sender', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('from_header', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('body_plain', self.gf('django.db.models.fields.TextField')()),
            ('stripped_text', self.gf('django.db.models.fields.TextField')()),
            ('stripped_signature', self.gf('django.db.models.fields.TextField')()),
            ('body_html', self.gf('django.db.models.fields.TextField')()),
            ('stripped_html', self.gf('django.db.models.fields.TextField')()),
            ('update', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mailgun_receiver.Update'])),
        ))
        db.send_create_signal(u'mailgun_receiver', ['Message'])

        # Adding model 'Attachment'
        db.create_table(u'mailgun_receiver_attachment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mailgun_receiver.Message'])),
            ('filename', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('content_type', self.gf('django.db.models.fields.TextField')(max_length=15)),
            ('f', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'mailgun_receiver', ['Attachment'])


    def backwards(self, orm):
        # Deleting model 'Update'
        db.delete_table(u'mailgun_receiver_update')

        # Deleting model 'Message'
        db.delete_table(u'mailgun_receiver_message')

        # Deleting model 'Attachment'
        db.delete_table(u'mailgun_receiver_attachment')


    models = {
        u'mailgun_receiver.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'content_type': ('django.db.models.fields.TextField', [], {'max_length': '15'}),
            'f': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Message']"})
        },
        u'mailgun_receiver.message': {
            'Meta': {'object_name': 'Message'},
            'body_html': ('django.db.models.fields.TextField', [], {}),
            'body_plain': ('django.db.models.fields.TextField', [], {}),
            'from_header': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'recipient': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripped_html': ('django.db.models.fields.TextField', [], {}),
            'stripped_signature': ('django.db.models.fields.TextField', [], {}),
            'stripped_text': ('django.db.models.fields.TextField', [], {}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'update': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Update']"})
        },
        u'mailgun_receiver.update': {
            'Meta': {'object_name': 'Update'},
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last': ('django.db.models.fields.DecimalField', [], {'max_digits': '13', 'decimal_places': '7'})
        }
    }

    complete_apps = ['mailgun_receiver']