=====
Django-mailgun-receiver
=====

Django-mailgun-receiver is a simple Django app to import e-mails by mailgun. For each messages are imported content and attachments 

Quick start
-----------

1. Add "mailgun_receiver" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'mailgun_receiver',
    )

2. Include the mailgun_receiver views in your project urls.py like this::

    url(r'^callback/', 'mailgun_receiver.views.callback'),

We recommend overwrite it with views which will delay execution `mailgun_receiver.utils.fetch` or `python manage.py get_msg` fg. add to queue.

3. Add Mailgun settings in your project settings.py like this::

    MAILGUN = {'DOMAIN':'pytamy.jawne.info.pl',
               'API_KEY':'key-xxxxxxxxxxxx

3. Run `python manage.py migrate` to create the mailgun_receiver models.

4. Add to you mailgun account rules like this::

    store(notify="http://mydomain.com/callback")

5. Receive signals post_save in your app and assosiate messages.