# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Message.date'
        db.alter_column(u'mailgun_receiver_message', 'date', self.gf('django.db.models.fields.DateTimeField')(null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Message.date'
        raise RuntimeError("Cannot reverse this migration. 'Message.date' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Message.date'
        db.alter_column(u'mailgun_receiver_message', 'date', self.gf('django.db.models.fields.CharField')(max_length=30))

    models = {
        u'mailgun_receiver.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'content_type': ('django.db.models.fields.TextField', [], {'max_length': '15'}),
            'f': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Message']"})
        },
        u'mailgun_receiver.message': {
            'Meta': {'object_name': 'Message'},
            'body_html': ('django.db.models.fields.TextField', [], {}),
            'body_plain': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'from_header': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message_headers': ('jsonfield.fields.JSONField', [], {'default': '{}'}),
            'message_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'recipients': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripped_html': ('django.db.models.fields.TextField', [], {}),
            'stripped_signature': ('django.db.models.fields.TextField', [], {}),
            'stripped_text': ('django.db.models.fields.TextField', [], {}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'to_header': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'update': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mailgun_receiver.Update']"})
        },
        u'mailgun_receiver.update': {
            'Meta': {'object_name': 'Update'},
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['mailgun_receiver']