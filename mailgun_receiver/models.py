from django.db import models
from jsonfield import JSONField


class Update(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return unicode(self.created_on)

    class Meta:
        get_latest_by = 'created_on'


class Message(models.Model):
    message_id = models.CharField(max_length=200)
    date = models.DateTimeField(blank=True, null=True)
    message_headers = JSONField()
    recipients = models.CharField(max_length=70)
    sender = models.CharField(max_length=70)
    to_header = models.CharField(max_length=70)
    from_header = models.CharField(max_length=70)
    subject = models.CharField(max_length=200)
    body_plain = models.TextField()
    stripped_text = models.TextField()
    stripped_signature = models.TextField()
    body_html = models.TextField()
    stripped_html = models.TextField()
    update = models.ForeignKey(Update)

    def __unicode__(self):
        return "%s (%s)" % (self.sender, self.subject)


class Attachment(models.Model):
    message = models.ForeignKey(Message)
    filename = models.CharField(max_length=50)
    content_type = models.TextField(max_length=15)
    f = models.FileField(upload_to='messages/attachment/%Y/%m/%d/')
